/* --- Checking Browser and showing banner if internet explorer is detected --- */

window.onload = function () {
  checkbrowser()
}

function checkbrowser () {
  if (navigator.userAgent.match(/Trident\/(\d+)/)) {
    document.body.innerHTML = '<div class="ie-warning">Sie benutzen einen nicht unterstützten Browser, weswegen einige Features eventuell nicht richtig funktionieren. Um an dieser Studie teilzunehmen, wechseln Sie bitte zu einem unterstützten Browser wie zum Beispiel Microsoft Edge, Mozilla Firefox, Google Chrome, Opera oder Apple Safari.</div>' +
      document.body.innerHTML
  }
}
