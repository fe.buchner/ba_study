window.$(function () {
  window.$('.progress').each(function () {
    var value = window.$(this).attr('data-value')
    var left = window.$(this).find('.progress-left .progress-bar')
    var right = window.$(this).find('.progress-right .progress-bar')

    if (value > 0) {
      if (value <= 50) {
        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
      } else {
        right.css('transform', 'rotate(180deg)')
        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
      }
    }
  })

  function percentageToDegrees (percentage) {
    return percentage / 100 * 360
  }
})
