import { isMobile, isTablet } from 'mobile-device-detect'

window.onload = function () {
  checkdevice()
}

function checkdevice () {
  if (isMobile || isTablet) {
    document.body.innerHTML = ''
    document.body.innerHTML = '<div id="nav">\n' +
      '<div class="row">\n' +
      '  <div class="col-12 col-md-6 vertical-align-center">\n' +
      '    <img src="/img/lmu_logo_hellgrau.3bc3f448.png" alt="lmu-logo" style="height: 50px; display: inline-flex;">\n' +
      '    <p class="mb-0 ml-2 d-inline-flex text-white">Vielen Dank für deinen Rat, XAI</p>\n' +
      '  </div>\n' +
      '  <div class="col-md-6">\n' +
      '  </div>\n' +
      '</div>\n' +
      '</div>\n' +
      '<div class="container my-5" id="mobileDevice">\n' +
      '      <div class="row">\n' +
      '        <div class="col-md-12">\n' +
      '          <div class="card">\n' +
      '            <div class="card-header font-weight-bold">Vielen Dank für Ihr Interesse an der Studie "Vielen Dank für deinen Rat,\n' +
      '              XAI"!\n' +
      '            </div>\n' +
      '            <div class="card-body">\n' +
      '              <b>Liebe Teilnehmerin, lieber Teilnehmer,</b>\n' +
      '              <br><br>\n' +
      '              hiermit möchten wir uns herzlich für Ihr Interesse an unserer Studie bedanken.' +
      '              Leider ist es aufgrund der Beschafftenheit der Studie nicht möglich diese auf einem mobilen Endgerät zu bearbeiten.' +
      '              Wir bitten dies zu entschuldigen und hoffen, dass Sie die Studie auf einem Laptop oder Desktop-PC fortsetzen.\n' +
      '              <br><br>\n' +
      '              Bei Fragen oder Problemen kontaktieren Sie uns gerne unter folgender Email-Adresse: <a\n' +
      '                href="mailto:fe.buchner@campus.lmu.de">fe.buchner@campus.lmu.de</a>' +
      '              <br><br>\n' +
      '              Vielen Dank für Ihr Verständnis!\n' +
      '            </div>\n' +
      '          </div>\n' +
      '        </div>\n' +
      '      </div>\n' +
      '    </div>' +
      document.body.innerHTML
  }
}
