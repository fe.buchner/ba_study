window.$(document).ready(function () {
  window.$('#3_textarea').on('keyup', function () {
    var words = this.value.match(/\S+/g).length
    if (words > 200) {
      // Split the string on first 200 words and rejoin on spaces
      var trimmed = window.$(this).val().split(/\s+/, 200).join(' ')
      // Add a space at the end to keep new typing making new words
      window.$(this).val(trimmed + ' ')
    } else {
      window.$('#display_count').text(words)
    }
  })
})
