import jQuery from 'jquery'
window.jQuery = window.$ = jQuery

window.$(document).ready(function () {
  window.$('[data-toggle="tooltip"]').tooltip()
  window.$('[data-toggle="popover"]').popover()
})
